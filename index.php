<?php
require "vendor/autoload.php";

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addFilter(new Twig_Filter('trad', function($string){
        return $string;
    }));
});

Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});

Flight::route('/', function(){
    $data = [
        'dinos' => recup_dinos(),
    ];
    Flight::render('dinosaures.twig', $data);
    Flight::render('dino.twig', $data);
});

Flight::route('/dinosaur/@slug', function($slug){
    $slug = [
        'dino' => display_dino($slug),
        //'random' => random(),
    ];
    Flight::render('dino.twig', $slug);
});

Flight::start();


?>