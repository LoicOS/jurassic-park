Un simple site web pour découvrir le pattern MVC avec une approche TDD via l'utilisation de:
Composer,
FlightPHP,
Twig,
Requests,
PHPUnit,
et bien d'autres choses ..
How to ?

composer install
vendor/bin/phpunit tests/
127.0.0.1:8000
Et go sur 127.0.0.1:8000
Tests ?

La couverture est minimal mais suffisante pour découvrir les T.U et d'intégrations avec PHPUnit.
Pour les lancer un simple: vendor/bin/phpunit tests/
Requirements

mikecao/flight
symfony/process
guzzlehttp/guzzle
phpunit/phpunit
twig/twig
rmccue/requests
michelf/php-markdown
Purpose

This project is only for educational purpose. It's actually a basic application for generating a dummy website.
Authors

Loic WALLON
