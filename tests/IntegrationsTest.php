<?php
require_once 'vendor/autoload.php';

class PagesIntegrationTest extends IntegrationTest{

    public function test_index()
    {
        $response = $this->make_request("GET", "/");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString("text/html", $response->getHeader('Content-Type')[0]);
    }


    public function test_pages()
	{	    
	    $response = $this->make_request("GET", "/dinosaur/{{ dino.slug }}");
        $this->assertEquals(200, $response->getStatusCode());
	    $this->assertStringContainsString("text/html", $response->getHeader('Content-Type')[0]);
	}
	
}
